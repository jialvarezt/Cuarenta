using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Card
{
    [field: SerializeField] public int CardValue { get;  set; }
    [field: SerializeField] public Suit CardSuit { get;  set; }

    public Card(int cardValue, Suit cardSuit)
    {
        CardValue = cardValue;
        CardSuit = cardSuit;
    }
}
