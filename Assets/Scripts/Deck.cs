using System;
using System.Collections.Generic;
using UnityEngine;
using Extensions;

public class Deck : MonoBehaviour
{
    private Stack<Card> _cards;

    private void Start()
    {
        ShuffleCards();
        while(_cards.Count != 0)
        {
            var card = _cards.Pop(); 
            Debug.Log($"card: {card.CardValue} {card.CardSuit}");
        }
    }

    private void ShuffleCards()
    {
        var allCards = new List<Card>();
        for (var i = 0; i < 10; i++)
        {
            allCards.Add(new Card(i,Suit.Clubs));
            allCards.Add(new Card(i,Suit.Diamonds));
            allCards.Add(new Card(i,Suit.Hearts));
            allCards.Add(new Card(i,Suit.Pikes));
        }
        allCards.Shuffle();
        _cards = new Stack<Card>(allCards); 
    }


}
